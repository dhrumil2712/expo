import React from 'react';
import { Button, Image, View, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation'; // Version can be specified in package.json


//for image 
class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('./spiro.png')}
        style={{ width: 30, height: 30 }}
      />
    );
  }
}

class WelcomeScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>Welcome to Smart Bank</Text>
        <Button
          title="Account Transfer"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('Home', {
              //itemId: 86,
              //otherParam: 'anything you want here',
            });
          }}
        />
      </View>
    );
  }
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>TCS</Text>
        <Button
          title="From Account"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('Details', {
              //itemId: 86,
              //otherParam: 'anything you want here',
            });
          }}
        />
      </View>
    );
  }
}

class DetailsScreen extends React.Component {

  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };


  //static navigationOptions = ({ navigation, navigationOptions }) => {
    //console.log(navigationOptions);
    // Notice the logs ^
    // sometimes we call with the default navigationOptions and other times
    // we call this with the previous navigationOptions that were returned from
    // this very function
    /*return {
      title: navigation.getParam('otherParam', 'A Nested Details Screen'),
      headerStyle: {
        backgroundColor: navigationOptions.headerTintColor,
      },
      headerTintColor: navigationOptions.headerStyle.backgroundColor,
    };
  };*/

  render() {
    /* 2. Get the param, provide a fallback value if not available */
    const { navigation } = this.props;
    //const itemId = navigation.getParam('itemId', 'NO-ID'); 
    //const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'space-between' }}>
        <Text>TO Account</Text>
        
        <Button style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}
          title="To Account"
          onPress={() =>
            this.props.navigation.push('Amount', {
             // itemId: Math.floor(Math.random() * 100),
            })}
        />
        
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Welcome')}
        />

        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

class AmountScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>Enter Amount</Text>
        <Button
          title="Amount"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('Customize', {
             // itemId: 86,
              //otherParam: 'anything you want here',
            });
          }}
        />
      </View>
    );
  }
}


class CustomizeScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>Customize your data</Text>
        <Button
          title="Customize"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('Conformation', {
             // itemId: 86,
              //otherParam: 'anything you want here',
            });
          }}
        />
      </View>
    );
  }
}



class ConformationScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>Customize your data</Text>
        <Button
          title="Conformation"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('ThankYou', {
             // itemId: 86,
              //otherParam: 'anything you want here',
            });
          }}
        />
      </View>
    );
  }
}

class ThankYouScreen extends React.Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text>Thank you for banking with us!!</Text>

        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Welcome')}
        />

        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />

      </View>
    );
  }
}

const RootStack = createStackNavigator(
  {
    Welcome:WelcomeScreen,
    Home: HomeScreen,
    Details: DetailsScreen,
    Amount:AmountScreen,
    Customize:CustomizeScreen,
    Conformation:ConformationScreen,
    ThankYou:ThankYouScreen,
  },
  {
    initialRouteName: 'Welcome',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

const AppContainer = createAppContainer(RootStack);




export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
